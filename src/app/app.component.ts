import { Component } from '@angular/core';
import { DataService } from './data.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'my-first-project';

  posts = [];

  name: string;
  age: number;
  adress: {

      street: string;
      city: string;

  };

  constructor(private dataService: DataService ) {

      this.dataService.getData().subscribe(data => {
        this.posts = data;

      });

  }

  hobbies: string[] = ['Video games', 'Soccer', 'Programing'];

alerting() {
  alert('Hola mundo');
}

  deleteHobby(hobby) {

    for (let index = 0; index < this.hobbies.length; index++) {


        if (hobby == this.hobbies[index]) {

          this.hobbies.splice(index, 1);

        }

    }


  }



  addHobby(newHobby) {
    this.hobbies.push(newHobby.value);
    newHobby.value = '';
    return false;

  }

}
